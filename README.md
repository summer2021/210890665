# Vineyard 

## Connecting Vineyard to Machine Learning Frameworks

### Frameworks:

- TensorFlow
- PyTorch
- MxNet
- Nvidia DALI

Pre Mid-Term:
- TensorFLow
- PyTorch

Post Mid-Term:
- MxNet
- Nvidia DALI

#### Contributor: Chaitravi Chalke
#### Mentor: Andy Diwen Zhu
